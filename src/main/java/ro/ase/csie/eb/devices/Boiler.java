package ro.ase.csie.eb.devices;

import ro.ase.csie.eb.observer.Observer;
import ro.ase.csie.eb.state.DeviceState;
import ro.ase.csie.eb.state.ErrorState;
import ro.ase.csie.eb.state.IdleState;
import ro.ase.csie.eb.state.WorkingState;

import java.util.Random;

public class Boiler extends Device {

    @Override
    public String comingHome(int minutes, Observer initiator) {
        if (!currentState.getStateName().equals(DeviceState.IDLE)) {
            return "Boiler: I'm already busy";
        }
        this.lastUser = initiator.getName();
        int rand = new Random().nextInt(10);
        if (rand > 8) {
            this.currentState = new ErrorState();
            return "Boiler: Something went terribly wrong";
        }
        this.currentState = new WorkingState();
        sleep();
        return "Boiler: Heating up";
    }

    @Override
    public String leavingHome(int minutes, Observer initiator) {
        if (!currentState.getStateName().equals(DeviceState.IDLE)) {
            return "Boiler: I'm already busy";
        }
        this.lastUser = initiator.getName();
        int rand = new Random().nextInt(10);
        if (rand > 8) {
            this.currentState = new ErrorState();
            return "Boiler: Something went terribly wrong";
        }
        this.currentState = new WorkingState();
        sleep();
        return "Boiler: Cooling down";
    }
}
