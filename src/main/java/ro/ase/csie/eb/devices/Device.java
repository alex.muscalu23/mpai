package ro.ase.csie.eb.devices;

import ro.ase.csie.eb.observer.Observer;
import ro.ase.csie.eb.state.DeviceState;
import ro.ase.csie.eb.state.IdleState;

public abstract class Device {

    protected DeviceState currentState;
    protected String lastUser;

    Device() {
        this.currentState = new IdleState();
    }

    public String getLastUser() {
        return lastUser;
    }

    public void setLastUser(String lastUser) {
        this.lastUser = lastUser;
    }

    public String getCurrentState() {
        return currentState.getStateName();
    }

    public abstract String comingHome(int minutes, Observer initiator);

    public abstract String leavingHome(int minutes, Observer initiator);

    protected void sleep() {
        new Thread( () -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.currentState = new IdleState();
            this.lastUser = "self";
        }).start();
    }
}
