package ro.ase.csie.eb.devices;

import ro.ase.csie.eb.observer.Observer;
import ro.ase.csie.eb.state.DeviceState;
import ro.ase.csie.eb.state.ErrorState;
import ro.ase.csie.eb.state.IdleState;
import ro.ase.csie.eb.state.WorkingState;

import java.util.Random;

public class Light extends Device {

    @Override
    public String comingHome(int minutes, Observer initiator) {
        if (!currentState.getStateName().equals(DeviceState.IDLE)) {
            return "Light: I'm already busy";
        }
        this.lastUser = initiator.getName();
        int rand = new Random().nextInt(10);
        if (rand > 8) {
            this.currentState = new ErrorState();
            return "Light: Something went terribly wrong";
        }
        this.currentState = new WorkingState();
        sleep();
        return "Light: Lighting em up";
    }

    @Override
    public String leavingHome(int minutes, Observer initiator) {
        if (!currentState.getStateName().equals(DeviceState.IDLE)) {
            return "Light: I'm already busy";
        }
        this.lastUser = initiator.getName();
        int rand = new Random().nextInt(10);
        if (rand > 8) {
            this.currentState = new ErrorState();
            return "Light: Something went terribly wrong";
        }
        this.currentState = new WorkingState();
        sleep();
        return "Light: Going dark";
    }
}
