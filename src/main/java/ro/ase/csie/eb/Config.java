package ro.ase.csie.eb;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ro.ase.csie.eb.devices.*;
import ro.ase.csie.eb.observer.House;

@Configuration
public class Config {
    @Bean
    House house(){
        House house = new House();
        house.addDevice(new AirConditioning())
                .addDevice(new Boiler())
                .addDevice(new WashingMachine())
                .addDevice(new Light())
                .addDevice(new Cleaner());

        new Thread(house).start();
        return  house;
    }

    @Bean
    HouseFacade houseFacade(House house){
        return new HouseFacade(house);
    }
}
