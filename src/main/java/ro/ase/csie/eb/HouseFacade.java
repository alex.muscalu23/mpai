package ro.ase.csie.eb;

import ro.ase.csie.eb.observer.House;
import ro.ase.csie.eb.observer.Observer;

import java.util.List;
import java.util.stream.Collectors;

public class HouseFacade {

    private House house;

    public HouseFacade(House house) {
        this.house = house;
    }

    public List<String> comingHome(int minutes, Observer initiator){
        return house.getDeviceStateMap().keySet()
                .stream()
                .map(d -> d.comingHome(minutes, initiator))
                .collect(Collectors.toList());
    }

    public  List<String> leavingHome(int minutes, Observer initiator) {
        return house.getDeviceStateMap().keySet()
                .stream()
                .map(d -> d.leavingHome(minutes, initiator))
                .collect(Collectors.toList());
    }
}
