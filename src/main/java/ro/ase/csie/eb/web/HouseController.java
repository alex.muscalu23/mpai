package ro.ase.csie.eb.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ro.ase.csie.eb.observer.House;
import ro.ase.csie.eb.HouseFacade;
import ro.ase.csie.eb.observer.Observer;

import java.util.List;

@RestController
@RequestMapping(value = "/house")
public class HouseController {
    @Autowired
    HouseFacade houseFacade;
    @Autowired
    House house;

    @GetMapping(value = "/comingHome")
    public List<String> comingHome(@RequestParam String name){
        Observer observer = house.getTenants().stream().filter(o->o.getName().equals(name)).findFirst().get();
        return houseFacade.comingHome(0, observer);
    }

    @GetMapping(value = "/leavingHome")
    public List<String> leavingHome(@RequestParam String name){
        Observer observer = house.getTenants().stream().filter(o->o.getName().equals(name)).findFirst().get();
        return houseFacade.leavingHome(0, observer);
    }
}
