package ro.ase.csie.eb.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ro.ase.csie.eb.Event;
import ro.ase.csie.eb.observer.House;
import ro.ase.csie.eb.observer.Observer;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

@Slf4j
@RestController()
@RequestMapping("/tenant")
public class TenantController {

    @Autowired
    House house;

    @GetMapping
    public void addTenant(@RequestParam String name, @RequestParam String type) {
        log.info(name);
        if (name == null) {
            throw new IllegalArgumentException("Name cannot be empty");
        }

        house.getTenants().stream().map(Observer::getName).filter(s -> s.equals(name)).findAny().ifPresent(s -> {
            throw new IllegalArgumentException("Name already registered");
        });

        house.subscribe(Observer.create(name, type));
    }

    @DeleteMapping
    public void removeTenant(@RequestParam String name) {
        log.info(name);
        Observer observer = house.getTenants().stream().filter(o->o.getName().equals(name)).findFirst().get();
        house.unsubscribe(observer);
    }


    @GetMapping(path = "/log")
    public List<Event> getEventLog(@RequestParam String name) {
        return house.getTenants().stream().filter(o -> o.getName().equals(name)).findFirst().get().getEventLog();

    }

    @ExceptionHandler({IllegalArgumentException.class, NullPointerException.class, NoSuchElementException.class})
    void handleBadRequests(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }
}
