package ro.ase.csie.eb;

public class Event {
    private String initiator;
    private String target;
    private String previousState;
    private String currentState;

    public Event(String initiator, String target, String previousState, String currentState) {
        this.initiator = initiator;
        this.target = target;
        this.previousState = previousState;
        this.currentState = currentState;
    }

    public String getInitiator() {
        return initiator;
    }

    public String getTarget() {
        return target;
    }

    public String getPreviousState() {
        return previousState;
    }

    public String getCurrentState() {
        return currentState;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder {
        private String initiator;
        private String target;
        private String previousState;
        private String currentState;

        private Builder() {
        }

        public Builder initiator(String initiator) {
            this.initiator = initiator;
            return this;
        }

        public Builder target(String target) {
            this.target = target;
            return this;
        }

        public Builder previousState(String previousState) {
            this.previousState = previousState;
            return this;
        }

        public Builder currentState(String currentState) {
            this.currentState = currentState;
            return this;
        }

        public Event build() {
            return new Event(initiator, target, previousState, currentState);
        }
    }
}
