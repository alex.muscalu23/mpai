package ro.ase.csie.eb.observer;

import ro.ase.csie.eb.Event;

import java.util.ArrayList;
import java.util.List;

public abstract class Observer {

    private final String name;
    protected List<Event> eventLog = new ArrayList<>();

    public Observer(String name) {
        this.name = name;
    }

    public static Observer create(String name, String type) {
        if ("owner".equalsIgnoreCase(type)) {
            return new Owner(name);
        }
        if ("guest".equalsIgnoreCase(type)) {
            return new Guest(name);
        }
        throw new IllegalArgumentException("Invalid type");
    }

    public List<Event> getEventLog() {
        return eventLog;
    }

    public String getName() {
        return name;
    }

    public abstract void receiveEvent(Event e);
}
