package ro.ase.csie.eb.observer;

import ro.ase.csie.eb.Event;

public class Owner extends Observer {

    public Owner(String name) {
        super(name);
    }

    @Override
    public void receiveEvent(Event e) {
        eventLog.add(e);
    }

}
