package ro.ase.csie.eb.observer;

import lombok.extern.slf4j.Slf4j;
import ro.ase.csie.eb.Event;
import ro.ase.csie.eb.devices.Device;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class House implements Runnable {
    public synchronized List<Observer> getTenants() {
        return tenants;
    }

    /*** OBSERVER ***/
    private List<Observer> tenants = new ArrayList<>();

    public void subscribe(Observer observer) {
        getTenants().add(observer);
    }

    public void unsubscribe(Observer observer) {
        getTenants().remove(observer);
    }

    /***************/

    public Map<Device, String> getDeviceStateMap() {
        return deviceStateMap;
    }

    private Map<Device, String> deviceStateMap = new HashMap<>();


    public House addDevice(Device device) {
        deviceStateMap.put(device, device.getCurrentState());
        return this;
    }


    @Override
    public void run() {
        log.info("House starting!");
        while (true) {
            Map<Device, String> newDeviceStateMap = new HashMap<>();
            deviceStateMap.forEach((d, s) -> {
                newDeviceStateMap.put(d, d.getCurrentState());
                if (!d.getCurrentState().equals(s)) {
                    // to do builder here
                    getTenants().forEach(t -> t.receiveEvent(Event.builder()
                            .initiator(d.getLastUser())
                            .currentState(d.getCurrentState())
                            .previousState(s)
                            .target(d.getClass().getSimpleName())
                            .build()
                    ));
                }
            });
            deviceStateMap = newDeviceStateMap;
            try {
                Thread.sleep(500);
                //log.info("looping");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
