package ro.ase.csie.eb.observer;

import ro.ase.csie.eb.Event;

public class Guest extends Observer {

    public Guest(String name) {
        super(name);
    }

    @Override
    public void receiveEvent(Event e) {
        if (e.getInitiator().equals(getName())) {
            eventLog.add(e);
        }
    }
}
