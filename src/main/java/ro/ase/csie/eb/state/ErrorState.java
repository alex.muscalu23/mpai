package ro.ase.csie.eb.state;

public class ErrorState implements DeviceState {
    @Override
    public String getStateName() {
            return ERROR;
    }
}
