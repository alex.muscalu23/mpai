package ro.ase.csie.eb.state;

public class IdleState implements DeviceState {

    @Override
    public String getStateName() {
        return IDLE;
    }
}
