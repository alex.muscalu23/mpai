package ro.ase.csie.eb.state;

public interface DeviceState {
    String IDLE = "IDLE";
    String ERROR = "ERROR";
    String WORKING = "WORKING";

    String getStateName();
}
