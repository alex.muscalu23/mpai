package ro.ase.csie.eb.state;

public class WorkingState implements DeviceState {

    @Override
    public String getStateName() {
        return WORKING;
    }
}
