export enum TenantType {
  owner = 'owner',
  guest = 'guest'
}
