import {environment} from '../../environments/environment';

import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

// rxjs
import {Observable} from 'rxjs/Observable';

// models
import {Tenant} from '../models/Tenant.model';

@Injectable()
export class TenantHttp {
  private api = environment.api;

  constructor(protected httpClient: HttpClient) {
  }

  public login(tenant: Tenant): Promise<any> {
    return this.httpClient.get(this.api + '/tenant', {
      params: new HttpParams()
        .set('name', tenant.name)
        .set('type', tenant.type)
    }).toPromise();
  }

  public logout(tenant: Tenant): Promise<any> {
    return this.httpClient.delete(this.api + '/tenant', {
      params: new HttpParams()
        .set('name', tenant.name)
    }).toPromise();
  }

  public log(tenant: Tenant): Promise<any> {
    return this.httpClient.get(this.api + '/tenant/log', {
      params: new HttpParams()
        .set('name', tenant.name)
    }).toPromise();
  }
}
