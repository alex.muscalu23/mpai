import {environment} from '../../environments/environment';

import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

// rxjs
import {Observable} from 'rxjs/Observable';

// models
import {Tenant} from '../models/Tenant.model';

@Injectable()
export class HouseHttp {
  private api = environment.api;

  constructor(protected httpClient: HttpClient) {
  }

  public leaving(tenant: Tenant): Promise<any> {
    return this.httpClient.get(this.api + '/house/leavingHome', {
      params: new HttpParams()
        .set('name', tenant.name)
    }).toPromise();
  }

  public coming(tenant: Tenant): Promise<any> {
    return this.httpClient.get(this.api + '/house/comingHome', {
      params: new HttpParams()
        .set('name', tenant.name)
    }).toPromise();
  }
}
