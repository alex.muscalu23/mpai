import {TenantType} from '../enums/TenantType.enum';

export class Tenant {
  name: string;
  type: TenantType;


  constructor(name: string, type: TenantType) {
    this.name = name;
    this.type = type;
  }
}
