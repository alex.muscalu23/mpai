import { Component, OnInit } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

import {Tenant} from '../../shared/models/Tenant.model';
import {TenantType} from '../../shared/enums/TenantType.enum';
import {TenantHttp} from '../../shared/http/tenant.http';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css'],
  providers: [CookieService, TenantHttp]
})
export class LogComponent implements OnInit {
  tenant: Tenant;
  response: any;

  constructor(private tenantHttp: TenantHttp, private cookieService: CookieService) {
    this.tenant = new Tenant(cookieService.get('tenant_name'), TenantType[cookieService.get('tenant_type')]);
  }

  ngOnInit() {
    this.tenantHttp
      .log(this.tenant)
      .then((response) => {
        this.response = response;
      })
      .catch((error) => {
        alert(error);
      });
  }

}
