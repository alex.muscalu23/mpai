import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';

import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';

import {FlexLayoutModule} from '@angular/flex-layout';
import {AppMaterialModule} from './material.module';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {HouseComponent} from './house/house.component';
import {LogComponent} from './log/log.component';
import {FormsModule} from '@angular/forms';

const ROUTES: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'house', component: HouseComponent},
  {path: 'log', component: LogComponent},
  {path: '**', redirectTo: '/login'}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HouseComponent,
    LogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, {useHash: true}),
    BrowserAnimationsModule,
    FlexLayoutModule,
    AppMaterialModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
