import {NgModule} from '@angular/core';

import {
  MatToolbarModule, MatIconModule,
  MatCardModule,
  MatInputModule, MatSelectModule,
  MatButtonModule,
  MatTooltipModule,
  MatListModule
} from '@angular/material';

@NgModule({
  imports: [
    MatToolbarModule, MatIconModule,
    MatCardModule,
    MatInputModule, MatSelectModule,
    MatButtonModule,
    MatTooltipModule,
    MatListModule
  ],
  exports: [
    MatToolbarModule, MatIconModule,
    MatCardModule,
    MatInputModule, MatSelectModule,
    MatButtonModule,
    MatTooltipModule,
    MatListModule
  ],
})

export class AppMaterialModule {
}
