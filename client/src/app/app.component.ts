import {Component} from '@angular/core';
import {Router} from '@angular/router';

import {CookieService} from 'ngx-cookie-service';

import {Tenant} from '../shared/models/Tenant.model';
import {TenantType} from '../shared/enums/TenantType.enum';
import {TenantHttp} from '../shared/http/tenant.http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CookieService, TenantHttp]
})
export class AppComponent {
  tenant: Tenant;

  constructor(private router: Router, private cookieService: CookieService, private tenantHttp: TenantHttp) {
  }

  public isLogin() {
    return this.router.url === '/login';
  }

  public logout() {
    this.tenant = new Tenant(this.cookieService.get('tenant_name'), TenantType[this.cookieService.get('tenant_type')]);

    this.tenantHttp.logout(this.tenant)
      .then((response) => {
        this.cookieService.delete('tenant_name');
        this.cookieService.delete('tenant_type');
        this.router.navigate(['/login']);
      })
      .catch((error) => {
        alert(error);
      });
  }
}
