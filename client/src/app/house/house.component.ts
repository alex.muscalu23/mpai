import {Component, OnInit} from '@angular/core';

import {CookieService} from 'ngx-cookie-service';
import {HouseHttp} from '../../shared/http/house.http';
import {Tenant} from '../../shared/models/Tenant.model';
import {TenantType} from '../../shared/enums/TenantType.enum';

@Component({
  selector: 'app-house',
  templateUrl: './house.component.html',
  styleUrls: ['./house.component.css'],
  providers: [CookieService, HouseHttp]
})
export class HouseComponent implements OnInit {
  tenant: Tenant;
  response: any;

  constructor(private houseHttp: HouseHttp, private cookieService: CookieService) {
    this.tenant = new Tenant(cookieService.get('tenant_name'), TenantType[cookieService.get('tenant_type')]);
  }

  public leaving() {
    this.houseHttp
      .leaving(this.tenant)
      .then((response) => {
        this.response = response;
      })
      .catch((error) => {
        alert(error);
      });
  }

  public coming() {
    this.houseHttp
      .coming(this.tenant)
      .then((response) => {
        this.response = response;
      })
      .catch((error) => {
        alert(error);
      });
  }

  ngOnInit() {
  }

}
