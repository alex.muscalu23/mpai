import {Component, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

import {Tenant} from '../../shared/models/Tenant.model';
import {TenantType} from '../../shared/enums/TenantType.enum';
import {TenantHttp} from '../../shared/http/tenant.http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [CookieService, TenantHttp]
})
export class LoginComponent implements OnInit {
  tenant: Tenant;
  tenantTypes: TenantType[];

  constructor(private tenantHttp: TenantHttp, private cookieService: CookieService, private router: Router) {
    this.tenant = new Tenant(null, TenantType.owner);
    this.tenantTypes = [
      TenantType.owner,
      TenantType.guest
    ];
  }

  public login() {
    if (!this.tenant.name) {
      return;
    }

    this.tenantHttp
      .login(this.tenant)
      .then((response) => {
        this.cookieService.set('tenant_name', this.tenant.name);
        this.cookieService.set('tenant_type', this.tenant.type);
        this.router.navigate(['/house']);
      })
      .catch((error) => {
        alert(error);
      });
  }

  ngOnInit() {
    if (this.cookieService.check('tenant_name')) {
      this.router.navigate(['/house']);
    }
  }

}
